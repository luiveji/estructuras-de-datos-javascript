/** Tema-2 Los Arrays */

/**
 * Una matriz de JavaScript es en realidad un tipo especializado de objeto de JavaScript, con los índices como nombres de propiedad que pueden ser enteros utilizados para representar desplazamientos. Sin embargo, cuando se utilizan enteros para los índices, se convierten internamente en cadenas para cumplir con los requisitos de los objetos JavaScript. Debido a que las matrices de JavaScript son solo objetos, no son tan eficientes como las matrices de otros lenguajes de programación.
 * 
 * Si bien las matrices de JavaScript son, estrictamente hablando, objetos de JavaScript, son objetos especializados categorizados internamente como matrices. La matriz es uno de los tipos de objetos JavaScript reconocidos y, como tal, hay un conjunto de propiedades y funciones que puede usar con las matrices.
 * 
 */

 // Voy crear un assert para ir familiarizandome con las pruebas

 function c(expected, correct){
     if( expected == correct) {
         console.log('true')
     }else{
         console.log('false')
     }
 }

 // Crear un array

 var personas = [];
 console.log(typeof personas); // Objeto

 // Como Objeto javascript "personas" tienes propieades y metodos disponibles nada mas declarar el array.

 // propiedad length->nos dice la longitud del array o el numero de elementos que contiene.

 console.log(personas.length) // 0

 /**
  * A diferencia de muchos otros lenguajes de programación, pero comunes para la mayoría de los lenguajes de secuencias de comandos, los elementos de la matriz de JavaScript no tienen que ser todos del mismo tipo:
  */

  // Acceder y escribir elemetos en un Array

  // 1- Escribir:

  var numeros = [];

  for (let i = 0; i <= 10; i++) {

        numeros[i] = i;
      
  }

  console.log(numeros);

  // 2- Acceder:
    var sum = 0;
  for (let index = 0; index < numeros.length; index++) {
     sum += numeros[index];
  }

  console.log(sum); //55

  // ARRAYS STRINGS 
  /**
   * La funcion split(@delimitador) ->  retorna un array, segun el delimitador que le hayamos pasado como argumento.
   */

   var saludo = 'Hoy hace un dia muy bonito';
   var a = saludo.split(' '); // retorna el array
   console.log(a.length)// tenemos 6 elementos en el array "a".



   // Crear una copia de un array

   function copyArray(a,b){
       for(var i = 0; i < a.length; i++) {
           b[i] = a[i]
       }

       return b;
   }

   var vacio = [];
   var names = ['Luis', 'Mihai', 'Raluca', 'Emilio'];
   console.log(names.length)
   console.log(copyArray(names, vacio));
   // Si muto array  "vacio", el cambio no se refleja en names.
   vacio[0] = 20;
   console.log(names) // vacio es objeto independiente de "names", por lo que no apuntan al mismo objeto;

   // FUNCIONES DE ACCESO

   /**
    * 
    *  IndexOf(@parametroABuscar) -> devuelve lo posicion en la que se encuentra la coincidencia entcontrada, si no
    *   devuelve -1.
    */

    c(names.indexOf('Mihai'), 1); // true

    /**
     * Si tiene múltiples ocurrencias de los mismos datos en una matriz, la función indexOf () siempre devolverá la posición de la primera aparición. Una función similar, lastIndexOf (), devolverá la posición de la última aparición del argumento en la matriz, o -1 si no se encuentra el argumento
     */


     // CREANDO NUEVOS ARRAYS DE ARRAYS

   /**
    * Hay dos funciones de acceso que le permiten crear nuevas matrices a partir de matrices existentes: concat () y splice (). La función concat () le permite juntar dos o más matrices para crear una nueva matriz, y la función splice () le permite crear una nueva matriz a partir de un subconjunto de una matriz existente.
    *  */     

    /**
     * La función splice () crea una nueva matriz a partir del contenido de una matriz existente. Los argumentos de la función son la posición inicial desde donde empezar a coger elementos y el segundo, cuantos elemento tomara a partir de ahi.
*/

   var nuevoArray = numeros.splice(3, 4);
   console.log(nuevoArray); // 3 4 5 6


   // MUTATOR FUNCTIONS

   /**
    *   Anadir elementos a un array, tenemos dos metodos:
        *   push(@elemento) -> lo anade al final del array.
        *   unshift(@element) -> lo anade al principio.
        * 
    *   Elminar elementos de un array, tenemos dos metodos:
    *       pop() -> elimina el ultimo elemento del array,
    *       shift() -> elemina el primer elemento del array
    * 
    *   Elimiar elementos en cualquier posicion de un array:
    *   splice(@dondeEmpiezo, @cuantosQuito) -> borrar
    *   splice(@donddEmpiezo, 0, elemetos que anado pasado por comas o  varible)
    */

    // Vamos a crear una funcion donde  busquemos un indice y luego lo eliminemos

    function eliminarElemento(arr, element) {
        var i = arr.indexOf(element);
        arr.splice(i, 1);
        return arr;
    }

    console.log(eliminarElemento(names, 'Raluca')) // Luis, Mihai, Emilio

    // ORDENAR ARRAYS
    /**
     *  reverse() -> ordena los elementos del array en orden inverso.
     *  sort() -> muchas opciones.
     * 
     */

     // FUNCIONES ITERADORAS
     /**
      * El conjunto final de funciones de matriz que examinaremos son funciones iteradoras. Estas funciones aplican una función a cada elemento de una matriz, ya sea devolviendo un valor, un conjunto de valores o una nueva matriz después de aplicar la función a cada elemento de una matriz.
      * 
      *  foreach(@element) -> no retorno  un array, simplemente paso una funcion a cada elemento del array.
      * 
      */

      numeros.forEach(function (element) {
          console.log(element * element);
      }); // todos los numeros al cuadrado;

      /**
       * reduce( @valorTotal, @valorActual) -> se utiliza como un acumulador de los elementos de un matriz.
       * Al principio el @valorTotal = 0; @valorActual = 1; por lo que el valor actual se actualizando en cada vuelta
       * de la iteracion.
       */

       var sumandos = numeros.reduce(function (inicio, element) {
           return inicio + element
       })

       console.log(sumandos);

       // ITERADORES QUE DEVUELVEN UN ARRAY.

       /**
        * Hay dos funciones iteradoras que devuelven nuevas matrices: map () y filter (). La función map () funciona como la función forEach (), aplicando una función a cada elemento de una matriz. La diferencia entre las dos funciones es que map () devuelve una nueva matriz con los resultados de la aplicación de la función.
        * 
        * 
        * La función filter () funciona de manera similar a every (), pero en lugar de devolver verdadero si todos los elementos de una matriz satisfacen una función booleana, la función devuelve una nueva matriz que consta de aquellos elementos que satisfacen la función booleana.
        */

       console.log('******  filter ****************')
        var n = [ 2, 3 ,7, 11, 87, 100];

        n.filter(function (element) {
            if(element % 2 == 0) {
                console.log(`${element} es par`);
            }else if (element % 3 == 0) {
                console.log(`${element} es impar`)
            }
        })